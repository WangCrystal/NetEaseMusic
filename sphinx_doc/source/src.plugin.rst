src.plugin package
==================

Subpackages
-----------

.. toctree::

    src.plugin.NetEase

Module contents
---------------

.. automodule:: src.plugin
    :members:
    :undoc-members:
    :show-inheritance:
