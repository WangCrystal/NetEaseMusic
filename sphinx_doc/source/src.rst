src package
===========

Subpackages
-----------

.. toctree::

    src.base
    src.plugin
    src.widgets

Submodules
----------

src.api module
--------------

.. automodule:: src.api
    :members:
    :undoc-members:
    :show-inheritance:

src.controllers module
----------------------

.. automodule:: src.controllers
    :members:
    :undoc-members:
    :show-inheritance:

src.left_widget module
----------------------

.. automodule:: src.left_widget
    :members:
    :undoc-members:
    :show-inheritance:

src.main module
---------------

.. automodule:: src.main
    :members:
    :undoc-members:
    :show-inheritance:

src.right_widget module
-----------------------

.. automodule:: src.right_widget
    :members:
    :undoc-members:
    :show-inheritance:

src.setting module
------------------

.. automodule:: src.setting
    :members:
    :undoc-members:
    :show-inheritance:

src.top_widget module
---------------------

.. automodule:: src.top_widget
    :members:
    :undoc-members:
    :show-inheritance:

src.views module
----------------

.. automodule:: src.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src
    :members:
    :undoc-members:
    :show-inheritance:
